From FEDORA_PATCHES Mon Sep 17 00:00:00 2001
From: Andrew Burgess <andrew.burgess@embecosm.com>
Date: Thu, 11 Apr 2024 14:05:46 -0700
Subject: gdb-rhel-10464-xsave-update-19of21.patch

;; Backport "bfd/binutils: support for gdb target descriptions in the core file"
;; (Andrew Burgess, RHEL-10464)

This commit lays the ground work for allowing GDB to write its target
description into a generated core file.

The goal of this work is to allow a user to connect to a remote
target, capture a core file from within GDB, then pass the executable
and core file to another user and have the user be able to examine the
state of the machine without needing to connect to a running target.

Different remote targets can have different register sets and this
information is communicated from the target to GDB in the target
description.

It is possible for a user to extract the target description from GDB
and pass this along with the core file so that when the core file is
used the target description can be fed back into GDB, however this is
not a great user experience.
It would be nicer, I think, if GDB could write the target description
directly into the core file, and then make use of this description
when loading a core file.

This commit performs the binutils/bfd side of this task, adding the
boiler plate functions to access the target description from within a
core file note, and reserving a new number for a note containing the
target description.  Later commits will extend GDB to make use of
this.

The new note is given the name 'GDB' and a type NT_GDB_TDESC.  This
should hopefully protect us if there's ever a reuse of the number
assigned to NT_GDB_TDESC by some other core file producer.  It should
also, hopefully, make it clearer to users that this note carries GDB
specific information.

diff --git a/bfd/elf-bfd.h b/bfd/elf-bfd.h
--- a/bfd/elf-bfd.h
+++ b/bfd/elf-bfd.h
@@ -2795,6 +2795,8 @@ extern char *elfcore_write_aarch_pauth
   (bfd *, char *, int *, const void *, int);
 extern char *elfcore_write_arc_v2
   (bfd *, char *, int *, const void *, int);
+extern char *elfcore_write_gdb_tdesc
+  (bfd *, char *, int *, const void *, int);
 extern char *elfcore_write_lwpstatus
   (bfd *, char *, int *, long, int, const void *);
 extern char *elfcore_write_register_note
diff --git a/bfd/elf.c b/bfd/elf.c
--- a/bfd/elf.c
+++ b/bfd/elf.c
@@ -9903,6 +9903,15 @@ elfcore_grok_arc_v2 (bfd *abfd, Elf_Internal_Note *note)
   return elfcore_make_note_pseudosection (abfd, ".reg-arc-v2", note);
 }
 
+/* Convert NOTE into a bfd_section called ".gdb-tdesc".  Return TRUE if
+   successful otherwise, return FALSE.  */
+
+static bfd_boolean
+elfcore_grok_gdb_tdesc (bfd *abfd, Elf_Internal_Note *note)
+{
+  return elfcore_make_note_pseudosection (abfd, ".gdb-tdesc", note);
+}
+
 #if defined (HAVE_PRPSINFO_T)
 typedef prpsinfo_t   elfcore_psinfo_t;
 #if defined (HAVE_PRPSINFO32_T)		/* Sparc64 cross Sparc32 */
@@ -10560,6 +10569,13 @@ elfcore_grok_note (bfd *abfd, Elf_Internal_Note *note)
       else
 	return TRUE;
 
+    case NT_GDB_TDESC:
+      if (note->namesz == 4
+          && strcmp (note->namedata, "GDB") == 0)
+        return elfcore_grok_gdb_tdesc (abfd, note);
+      else
+        return TRUE;
+
     case NT_PRPSINFO:
     case NT_PSINFO:
       if (bed->elf_backend_grok_psinfo)
@@ -11941,6 +11957,23 @@ elfcore_write_arc_v2 (bfd *abfd,
 			     note_name, NT_ARC_V2, arc_v2, size);
 }
 
+/* Write the target description (a string) pointed to by TDESC, length
+   SIZE, into the note buffer BUF, and update *BUFSIZ.  ABFD is the bfd the
+   note is being written into.  Return a pointer to the new start of the
+   note buffer, to replace BUF which may no longer be valid.  */
+
+char *
+elfcore_write_gdb_tdesc (bfd *abfd,
+                        char *buf,
+                        int *bufsiz,
+                        const void *tdesc,
+                        int size)
+{
+  const char *note_name = "GDB";
+  return elfcore_write_note (abfd, buf, bufsiz,
+                             note_name, NT_GDB_TDESC, tdesc, size);
+}
+
 char *
 elfcore_write_register_note (bfd *abfd,
 			     char *buf,
@@ -12025,6 +12058,8 @@ elfcore_write_register_note (bfd *abfd,
     return elfcore_write_aarch_pauth (abfd, buf, bufsiz, data, size);
   if (strcmp (section, ".reg-arc-v2") == 0)
     return elfcore_write_arc_v2 (abfd, buf, bufsiz, data, size);
+  if (strcmp (section, ".gdb-tdesc") == 0)
+    return elfcore_write_gdb_tdesc (abfd, buf, bufsiz, data, size);
   return NULL;
 }
 
diff --git a/include/elf/common.h b/include/elf/common.h
--- a/include/elf/common.h
+++ b/include/elf/common.h
@@ -665,6 +665,10 @@
 #define NT_SIGINFO	0x53494749	/* Fields of siginfo_t.  */
 #define NT_FILE		0x46494c45	/* Description of mapped files.  */
 
+/* The range 0xff000000 to 0xffffffff is set aside for notes that don't
+   originate from any particular operating system.  */
+#define NT_GDB_TDESC   0xff000000      /* Contains copy of GDB's target description XML.  */
+
 /* Note segments for core files on dir-style procfs systems.  */
 
 #define NT_PSTATUS	10		/* Has a struct pstatus */
